import './App.css';
import {Redirect, Route, Switch} from "react-router-dom";
import {useEffect, useState} from "react";
import SiteLoader from "./components/SiteLoader/SiteLoader";
import Home from "./pages/Home/Home";
import axios from "axios";
import {PATH_NAME} from "./tools/constant";
import Quiz from "./pages/Quiz/Quiz";
import Result from "./pages/Result/Result";
import {ToastContainer} from "react-toastify";
import PrivateRoute from "./routes/PrivateRoute";

function App() {

    const [site_loading, setSiteLoading] = useState(true);

    useEffect(() => {
        setTimeout(()=>{
            setSiteLoading(false);
        },1000);
    }, []);

    if (site_loading) {
        return (
            <SiteLoader/>
        )
    } else{
        return (
            <>
                <div className="App">

                    <Switch>

                        <Route exact path="/" component={Home}/>

                        <PrivateRoute path="/quiz" component={Quiz}/>
                        <PrivateRoute path="/result" component={Result}/>

                    </Switch>

                </div>

                <ToastContainer/>

            </>
        );
    }

}

export default App;
