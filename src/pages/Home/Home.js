import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';
import {getQuestions} from "../../redux/actions/quizActions";
import {AvField, AvForm} from "availity-reactstrap-validation";
import Categories from "../../data/Categories";
import FlagIcon from '@mui/icons-material/Flag';
import QuizLOgo from '../../images/quiz.png';
import QuizIcon from '@mui/icons-material/Quiz';

const Home = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {loading} = useSelector(state => state.quiz);

    const startQuiz = (event, value) => {
        window.scroll(0,0);
        dispatch(getQuestions(value, history));
    };

    return (


        <div className="Home">
            <div className="container vh-100">
                <div className="row h-100">

                    <div className="col-md-4 offset-lg-2 left-col">
                        <div className="d-flex h-100 align-items-center justify-content-center">
                            <div className="card w-100">

                                <div className="card-body p-0">

                                    <img src={QuizLOgo} alt="" className="img-fluid"/>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4 h-100 right-col">
                        <div className="d-flex h-100 align-items-center justify-content-center">
                            <div className="card w-100">

                                <div className="card-header bg-info">
                                    <h3 className="text-center text-white d-flex align-items-center justify-content-center">
                                        <span className="me-2">INTUITIVE QUIZ HUB</span>
                                        <QuizIcon/>
                                    </h3>
                                </div>

                                <div className="card-body">


                                    <AvForm className="w-100" onValidSubmit={startQuiz}>

                                        <AvField
                                            type="text"
                                            label="Username:"
                                            name="username"
                                            className="form-control-sm mb-3"
                                            required
                                        />

                                        <AvField
                                            type="number"
                                            label="Number of Questions:"
                                            name="amount"
                                            className="form-control-sm mb-3"
                                            min={1}
                                            required
                                        />

                                        <AvField
                                            type="select"
                                            label="Select Category:"
                                            name="category"
                                            className="form-select-sm mb-3"
                                        >
                                            <option value="" selected={true}>Any category</option>

                                            {
                                                Categories.map((item, index)=>(
                                                    <option value={item.value}>{item.category}</option>
                                                ))
                                            }

                                        </AvField>

                                        <AvField
                                            type="select"
                                            label="Select difficulty:"
                                            name="difficulty"
                                            className="form-select-sm mb-3"
                                        >
                                            <option value="easy">Easy</option>
                                            <option value="" selected={true}>Any difficulty</option>

                                            <option value="medium">Medium</option>
                                            <option value="hard">Hard</option>

                                        </AvField>

                                        <AvField
                                            type="select"
                                            label="Select type:"
                                            name="type"
                                            className="form-select-sm mb-3"
                                        >
                                            <option value="" selected={true}>Any type</option>

                                            <option value="multiple">Multiple Choise</option>
                                            <option value="boolean">True / False</option>

                                        </AvField>


                                        <div className="btn-box d-flex align-items-center justify-content-center mt-5">
                                            <button disabled={loading} type="submit" className="btn btn-info text-white w-50 d-flex align-items-center justify-content-around" >
                                                <span>START QUIZ</span> <FlagIcon/>
                                            </button>
                                        </div>


                                    </AvForm>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    );
};

export default Home;