import React, {useState, useEffect} from 'react';
import QuizTabs from "../../components/QuizTabs/QuizTabs";
import {useSelector, useDispatch} from "react-redux";
import {calculateResult} from "../../redux/actions/quizActions";
import {useHistory} from 'react-router-dom';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import HourglassTopIcon from '@mui/icons-material/HourglassTop';
import Timer from "../../components/Timer/Timer";
import FinishModal from "../../components/FinishModal/FinishModal";
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import CreditScoreIcon from '@mui/icons-material/CreditScore';
import {RESTART} from "../../redux/actionTypes/actionTypes";

const Quiz = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    const [showModal, setShowModal] = useState(false);

    const handleShow = () => setShowModal(true);
    const handleClose = () => setShowModal(false);

    const [currentActiveTab, setCurrentActiveTab] = useState(0);

    const {questions_amount, answers, questions, username, result} = useSelector(state => state.quiz);

    const handleNext = () => {
        if (currentActiveTab !== questions_amount - 1) {
            setCurrentActiveTab(currentActiveTab => currentActiveTab + 1);
        }
    };

    const handlePrev = () => {
        if (currentActiveTab !== 0) {
            setCurrentActiveTab(currentActiveTab => currentActiveTab - 1);
        }
    };

    const handleFinish = () => {

        let score = 0;

        questions.map((question, index) => {
            if (question.correct_answer === answers[index]?.answer) {
                score += 1;
            }
        });

        let result = {
            scoreBall: score,
            scorePoint: score * 100 / questions_amount
        };

        setTimeout(() => {
            dispatch(calculateResult(result, history));
        }, [1000]);

    };

    const viewResult = () => {
        history.push('/result');
    };

    const reStart = () => {

        setTimeout(()=>{
            dispatch({
                type: RESTART
            })
        },[1000]);
    };

    return (
        <div className="quiz">
            <div className="container my-4">

                <div className="d-flex align-items-center justify-content-center mb-3">
                    <h3 className="mobile-username">
                        <b className="text-info">Username: </b>{username && username}

                        {
                            result ? <h4 className="text-center text-info fw-bold mt-5">Answers</h4> : ""
                        }

                    </h3>
                </div>

                <div className="top d-flex align-items-center justify-content-between">

                    {
                        !result ?

                            <div className="me-3 d-flex align-items-center">
                                <div className="me-2 text-danger fw-bold">Finish:</div>
                                <span className="bg-info text-white p-2 d-flex align-items-center time-box">
                                    <HourglassTopIcon className="me-2"/>
                                    <Timer handleFinish={handleFinish}/>
                                </span>
                            </div>

                            :

                            <div className="me-3 d-flex align-items-center desktop-answer-box">
                                <h4 className="me-2 text-info fw-bold">Answers:</h4>
                            </div>

                    }

                    <h3 className="desktop-username">
                        <b className="text-info">Username: </b>{username && username}
                    </h3>

                    {
                        !result ?

                            <button className="btn btn-danger" onClick={handleShow}>
                                <span>Finish the QUIZ</span> <CheckCircleIcon/>
                            </button> :


                            <div className="d-flex align-items-center mobile-top-button-box">
                                <button className="me-2 btn btn-info text-white d-flex align-items-center justify-content-center" onClick={viewResult}>
                                    <span>VIEW RESULT</span> <CreditScoreIcon className="ms-1"/>
                                </button>
                                <button className="btn btn-info text-white d-flex align-items-center justify-content-center" onClick={reStart}>
                                    <span>RESTART the QUIZ</span> <RestartAltIcon className="ms-1"/>
                                </button>
                            </div>

                    }

                </div>

                <div className="center">
                    <QuizTabs currentActiveTab={currentActiveTab} setCurrentActiveTab={setCurrentActiveTab}/>
                </div>

                <div className="bottom d-flex align-items-center justify-content-between mt-4">
                    <button
                        className={`btn btn-info text-white d-flex align-items-center 
                                    ${currentActiveTab === 0 ? "not-allowed-button" : ""}
                                    `}
                        onClick={handlePrev}
                        disabled={currentActiveTab === 0 ? true : false}
                    >
                        <ArrowBackIosNewIcon className="me-1"/>
                        <div>PREV</div>
                    </button>
                    <button
                        onClick={handleNext}
                        disabled={currentActiveTab === (questions_amount - 1) ? true : false}
                        className={`btn btn-info text-white d-flex align-items-center 
                                    ${currentActiveTab === (questions_amount - 1) ? "not-allowed-button" : ""}
                                    `}
                    >
                        <div>NEXT</div>
                        <ArrowForwardIosIcon className="ms-1"/>
                    </button>
                </div>
            </div>

            <FinishModal handleClose={handleClose} showModal={showModal} handleFinish={handleFinish}/>

        </div>
    );
};

export default Quiz;