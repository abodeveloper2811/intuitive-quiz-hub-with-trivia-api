import React from 'react';
import {useSelector} from "react-redux";
import QuizImg from '../../images/quiz.png';
import {useHistory} from "react-router-dom";

const Result = () => {

    const {result, questions_amount, category, difficulty, username}  = useSelector(state => state.quiz);

    const history = useHistory();

    const viewAnswers =()=>{
        history.push('/quiz');
    };

    return (
        <div className="Result">
            <div className="container vh-100">
                <div className="row h-100">
                    <div className="col-md-4 offset-lg-4 h-100">
                        <div className="d-flex h-100 align-items-center justify-content-center">
                            <div className="card w-100">

                                <div className="card-header p-0">
                                    <img src={QuizImg} alt="" className="img-fluid"/>
                                </div>

                                <div className="card-body">

                                    <h3 className="score text-info text-center mt-2">YOUR RESULT</h3>
                                    <h1 className="score text-info text-center my-3"><b>{result.scoreBall} ball</b></h1>


                                    <div>
                                        <div className="d-flex align-items-center mb-2">
                                            <div className="me-2 text-info"><b>Username:</b></div>
                                            <div className="">{username}</div>
                                        </div>

                                        <div className="d-flex align-items-center">
                                            <div className="me-2"><b>Category:</b></div>
                                            <div className="">{category}</div>
                                        </div>

                                        <div className="d-flex align-items-center">
                                            <div className="me-2"><b>Level of difficulty:</b></div>
                                            <div className="">{difficulty}</div>
                                        </div>

                                        <div className="d-flex align-items-center">
                                            <div className="me-2"><b>Number of questions:</b></div>
                                            <div className="">{questions_amount}</div>
                                        </div>

                                        <div className="d-flex align-items-center">
                                            <div className="me-2"><b>Number of correct answers:</b></div>
                                            <div className="">{result.scoreBall}</div>
                                        </div>

                                        <div className="d-flex align-items-center mt-3">
                                            <div className="me-2 text-info"><b>Score:</b></div>
                                            <div className="">{result.scorePoint} %</div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div className="btn-box d-flex align-items-center justify-content-center mt-4">
                                        <button className="btn btn-info text-white" onClick={()=>viewAnswers()}>VIEW ANSWERS</button>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Result;