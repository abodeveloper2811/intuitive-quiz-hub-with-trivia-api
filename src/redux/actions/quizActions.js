import {CALCULATE_RESULT, GET_QUESTIONS, LOADING_FALSE, LOADING_TRUE, SET_ANSWER} from "../actionTypes/actionTypes";
import axios from "axios";
import {PATH_NAME} from "../../tools/constant";
import {toast} from "react-toastify";

export const getQuestions = (formData, history) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`${PATH_NAME}?amount=${formData.amount}&category=${formData.category}&difficulty=${formData.difficulty}&type=${formData.type}`);

        let questions = res.data.results;

        let myData = [];

        questions.map((question, index) => {

            let singleQuestionOptions = [];

            singleQuestionOptions = [
                question.correct_answer,
                ...question.incorrect_answers
            ];

            function shuffle(array) {
                let currentIndex = array.length, randomIndex;

                // While there remain elements to shuffle.
                while (currentIndex != 0) {

                    // Pick a remaining element.
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex--;

                    // And swap it with the current element.
                    [array[currentIndex], array[randomIndex]] = [
                        array[randomIndex], array[currentIndex]];
                }

                return array;
            }

            shuffle(singleQuestionOptions);

            let newObject = {};

            newObject = {
                ...question,
                options: singleQuestionOptions
            };
            myData.push(newObject);
        });

        let category = formData.category === '' ? "Any category" : questions[0].category;
        let difficulty = formData.difficulty === '' ? "Any difficulty" : questions[0].difficulty;

        let timer = 0;

        switch (difficulty) {

            case "Any difficulty":
                timer = myData.length * 60000;
                break;

            case "easy":
                timer = myData.length * 45000;
                break;

            case "medium":
                timer = myData.length * 60000;
                break;

            case "hard":
                timer = myData.length * 90000;
                break;
        }

        dispatch({
            type: GET_QUESTIONS,
            payload: {
                myData,
                username: formData.username,
                questions_amount: myData.length,
                category: category,
                difficulty: difficulty,
                timer: {
                    seconds: timer
                }
            }
        });

        history.push('/quiz');

    } catch (err) {
        dispatch({
            type: LOADING_FALSE,
        });

        toast.warning("Iltimos savollar sonini kamaytiring !!!");
        history.push('/');
    }

};

export const setAnswer = (obj, currentQuestion) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        dispatch({
            type: SET_ANSWER,
            payload: {
                obj,
                currentQuestion
            },
        });

    } catch (err) {
        dispatch({
            type: LOADING_FALSE,
        });
    }

};

export const calculateResult = (result, history) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        dispatch({
            type: CALCULATE_RESULT,
            payload: {
                result
            },
        });

        history.push('/result');

        window.scroll(0,0);

    } catch (err) {
        dispatch({
            type: LOADING_FALSE,
        });
    }

};


