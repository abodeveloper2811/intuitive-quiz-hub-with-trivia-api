import {
    CALCULATE_RESULT,
    GET_QUESTIONS,
    LOADING_FALSE,
    LOADING_TRUE,
    RESTART,
    SET_ANSWER
} from "../actionTypes/actionTypes";

const initialState = {
    questions: [],
    answers: [],
    username: null,
    questions_amount: null,
    category: null,
    difficulty: null,
    result: null,
    time_used: null,
    loading: false,
};


export const quizReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case LOADING_TRUE:
            return {
                ...state,
                loading: true,
            };

        case LOADING_FALSE:

            return {
                ...state,
                loading: false,
            };

        case GET_QUESTIONS:

            return {
                ...state,
                questions: payload.myData,
                username: payload.username,
                questions_amount: payload.questions_amount,
                category: payload.category,
                difficulty: payload.difficulty,
                timer: payload.timer,
                loading: false,
            };

        case SET_ANSWER:

            let newAnswersData = [...state.answers];
            newAnswersData[payload.currentQuestion] = payload.obj;

            return {
                ...state,
                answers: newAnswersData,
                loading: false,
            };


        case CALCULATE_RESULT:

            return {
                ...state,
                result: payload.result,
                loading: false,
            };

        case RESTART:

            return {
                ...state,
                questions: [],
                answers: [],
                username: null,
                questions_amount: null,
                category: null,
                difficulty: null,
                result: null,
                loading: false,
            };

        default:
            return state;
    }
};


