import {Redirect, Route} from "react-router-dom";
import {useSelector} from "react-redux";

const PrivateRoute = ( {path, component} ) => {

    const {username, result} = useSelector(state => state.quiz);

    switch (path) {

        case "/quiz":

            if(username !== null) {
                return <Route to={path} component={component}/>;
            }
            else {
                return <Redirect to="/" />
            }

            break;

        case "/result":

            if(username !== null) {

                if (result){
                    return <Route to={path} component={component}/>;
                } else{
                    return <Redirect to="/quiz" />
                }
            }
            else {
                return <Redirect to="/" />
            }

            break;

    }

};

export default PrivateRoute;