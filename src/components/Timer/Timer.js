import React from 'react';
import Countdown, {zeroPad} from 'react-countdown';
import {useDispatch, useSelector} from "react-redux";

const Timer = ({handleFinish}) => {

    const dispatch = useDispatch();

    const {timer} = useSelector(state => state.quiz);

    const time = React.useMemo(() => {
        return Date.now() + timer?.seconds;
    }, []);

    const Completionist = () => <span className="text-danger fw-bold">Time is up !!!</span>;

    const renderer = ({ hours, minutes, seconds, completed, total }) => {

        if (completed) {

            setTimeout(()=>{
                handleFinish();
            },[1000]);

            // Render a completed state
            return <Completionist />;
        } else {

            // Render a countdown
            return <span className="fw-bold">{zeroPad(hours)}:{zeroPad(minutes)}:{zeroPad(seconds)}</span>;
        }
    };

    return (
        <div>
            <Countdown
                date={time}
                renderer={renderer}
            />
        </div>
    );
};

export default Timer;