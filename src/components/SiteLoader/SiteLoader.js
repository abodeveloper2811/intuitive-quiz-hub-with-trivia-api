import React from 'react';
import { SemipolarLoading } from 'react-loadingg';


function SiteLoader(props) {
    return (
        <div className="SiteLoader">
            <SemipolarLoading size={"large"} color={"#0DCAF0"}/>
        </div>
    );
}

export default SiteLoader;