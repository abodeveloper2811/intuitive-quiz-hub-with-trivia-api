import React, {useState} from 'react'
import {Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import {useSelector, useDispatch} from "react-redux";
import {setAnswer} from "../../redux/actions/quizActions";
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import {decode} from "html-entities";

function QuizTabs({currentActiveTab, setCurrentActiveTab}) {

    const {questions, answers, result} = useSelector(state => state.quiz);

    const toggle = tab => {
        if (currentActiveTab !== tab) setCurrentActiveTab(tab);
    };

    const dispatch = useDispatch();

    const handleChange = (event) => {

        const {name, value} = event.target;

        let newObj = {
            answer: value
        };

        dispatch(setAnswer(newObj, currentActiveTab));
    };

    return (
        <div className="my-4">

            <Nav tabs>

                <NavItem className="d-flex">

                    {
                        questions.map((question, index) => (
                            <NavLink
                                className={`
                                
                                ${answers[index] ? "selected text-white" : ""} 
                                ${currentActiveTab === index ? "active-question active" : ""}
                                
                                ${result ? question.correct_answer === answers[index]?.answer ? "correct-answer" : "incorrect-answer" : ""}
                                
                                `}
                                onClick={() => {
                                    toggle(index);
                                }}
                            >
                                {index + 1}
                            </NavLink>
                        ))
                    }

                </NavItem>

            </Nav>

            <TabContent activeTab={currentActiveTab}>

                {
                    questions.map((question, questionIndex) => (
                        <TabPane tabId={questionIndex}>

                            <div className="row my-4">
                                <div className="col-md-12">
                                    <h4 className="text-info text-center mb-4">
                                        Question {currentActiveTab + 1}

                                        <h5 className="mt-1 category text-warning">({question.category})</h5>
                                    </h4>
                                </div>

                                <div className="col-md-12">

                                    <h6 className="question text-center mb-3 p-3">
                                       <b>
                                           {decode(question.question)}
                                       </b>
                                    </h6>

                                    <div className="row">

                                        {
                                            !result && question.options.map((option, optionIndex) => (

                                                <div className="col-md-12 my-2" key={optionIndex}>
                                                    <div className="form-check">

                                                        <input
                                                            type="radio" id={option}
                                                            name={`question${questionIndex}`}
                                                            value={option}
                                                            class="form-check-input"
                                                            onChange={handleChange}
                                                        />

                                                        <label htmlFor={option} class="form-check-label">
                                                            {decode(option)}
                                                        </label>

                                                    </div>


                                                </div>
                                            ))
                                        }

                                        <div className="no-answer-box">
                                            {
                                                result && !answers[questionIndex]?.answer ?
                                                    <div className="text-warning">No answer is selected !!!</div> : ""

                                            }
                                        </div>

                                        {
                                            result && question.options.map((option, optionIndex) => (
                                                <div className="col-md-12 my-2" key={optionIndex}>
                                                    <div className="form-check">

                                                        <input
                                                            type="radio" id={option}
                                                            value={option}
                                                            className="form-check-input"
                                                            checked={answers[questionIndex]?.answer === option ? true : false}
                                                        />

                                                        <label htmlFor={option}
                                                               className={`form-check-label ps-2 pe-1 ${answers[questionIndex]?.answer === option && question.correct_answer !== option 
                                                                   ? "bg-danger text-white rounded" 
                                                                   : question.correct_answer === option ? "bg-success rounded text-white" : ""}`}

                                                        >
                                                            {decode(option)}
                                                            {
                                                                 question.correct_answer === option ?

                                                                    <CheckIcon className="ms-1"/> : ""
                                                            }
                                                            {
                                                                answers[questionIndex]?.answer === option && question.correct_answer !== option ?

                                                                    <ClearIcon className="ms-1"/> : ""
                                                            }

                                                        </label>

                                                    </div>


                                                </div>
                                            ))
                                        }

                                    </div>

                                </div>
                            </div>


                        </TabPane>
                    ))
                }

            </TabContent>


        </div>
    );
}

export default QuizTabs;
