import React from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {useDispatch} from "react-redux";

const FinishModal = ({showModal, handleClose, handleFinish}) => {

    const dispatch = useDispatch();

    return (
        <div>

            <Modal isOpen={showModal} toggle={handleClose}>
                <ModalHeader toggle={handleClose} className="bg-danger text-white">Finish the QUIZ</ModalHeader>
                <ModalBody>
                    Are you sure you want to finish the <b>QUIZ</b> ?
                </ModalBody>
                <ModalFooter>
                    <button onClick={handleClose} className="btn btn-info text-white">NO</button>
                    <button onClick={handleFinish} className="btn btn-danger text-white">YES</button>
                </ModalFooter>
            </Modal>

        </div>
    );
};

export default FinishModal;